"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

import os
import csv
import flask
from flask_restful import Resource, Api, reqparse
from flask import request,  jsonify, send_file, safe_join
import arrow # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
from pymongo import MongoClient
import logging

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY
api = Api(app)
client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client['brevet_db']
collection = db['brevets']
parser = reqparse.RequestParser()
cwd = os.getcwd()

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    return flask.render_template('calc.html')

@app.route("/display")
def display_brevets():
    results = collection.find()
    found = []
    for result in results:
        db_controls = result['controls']
        for control in db_controls:
            found.append(control)

    if found:
        return flask.render_template('display.html', found=found )
    else:
        return flask.render_template('empty_db.html')

@app.route('/empty')
def empty_post_page():
    return flask.render_template('empty_post.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# Request handlers
#
###############

#Dict to be inserted into db
brevet ={}
brevet['ObjectId'] = 0

#list of controls in the brevet
controls = []


@app.route('/_submit', methods = ['POST'])
def _submit_db():
    
    if controls:
        
        brevet['controls']= controls

        collection.insert_one(brevet)

        #set up for next sumbit
        last_id = brevet['ObjectId']
        brevet.clear()
        controls.clear()
        brevet['ObjectId'] = last_id + 1
        return "200"
    else:
        app.logger.warning("empty")
        return flask.render_template('empty_post.html')


@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    #get request data
    km = request.args.get('km', 999, type=float)
    brevet_dist = request.args.get('brevet_dist',400, type=int)
    start_date = request.args.get('start_date','2021-12-21', type=str)
    start_time = request.args.get('start_time','12:00',type=str)

    #form arrow time object from form date and time 
    brevet_start = arrow.get(start_date + ' '+ start_time +':00', 'YYYY-MM-DD HH:mm:ss')

    #request the open and close times from acp_times
    open_time = acp_times.open_time(km, brevet_dist, brevet_start.isoformat())
    close_time = acp_times.close_time(km, brevet_dist, brevet_start.isoformat())

    controls.append({'open': open_time})
    controls.append({'close': close_time})

    brevet['brevet_dist'] = brevet_dist
    brevet['start_date'] = start_date
    brevet['start_date'] = start_date

    #returning the caluclated control times
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)


#############


###
# Schemas
###

#list all
class List_all(Resource):
    def get(self):
        list_f = list(collection.find())
        
        for db_record in list_f:
            db_record.pop('ObjectId')
            db_record.pop('_id')

        return jsonify(list_f)

class List_all_json(Resource):
    def get(self):
        return List_all.get(self)

class List_all_csv(Resource):
    def get(self):
        list_f = list(collection.find())
        rows_list = [['brevet_dist','start_date','controls']]
        for db_record in list_f:
            db_record.pop('ObjectId')
            db_record.pop('_id')

            row = [db_record.pop('brevet_dist'), db_record.pop('start_date')]
            c = db_record.pop("controls")

            for time in c:
                if time.get('open') is not None:
                    row.append(time.get('open'))
            for time in c:
                if time.get('close') is not None:
                    row.append(time.get('close'))
            rows_list.append(row)

        with open('brevets_all.csv', 'w', newline='') as file:
            csv_writer = csv.writer(file)
            csv_writer.writerows(rows_list)

        file_path = safe_join(cwd, 'brevets_all.csv')

        return send_file(file_path,
                        mimetype = "application/octet-stream",
                        as_attachment=True,
                        download_name = 'brevets_all.csv'
                        )
#open times
class List_open(Resource):
    def get(self):
        list_f = list(collection.find())
        open_times = []
        for db_record in list_f:
            c = db_record.pop("controls")
            for time in c:
                if time.get('open') is not None:
                    open_times.append(time.get('open'))
                               
        return jsonify(open_times)

class List_open_json(Resource):
    def get(self):
        parser.add_argument('top', type=int)
        query_num = parser.parse_args()["top"]

        list_f = list(collection.find())
        open_times = []
        for db_record in list_f:
            c = db_record.pop("controls")
            for time in c:
                if time.get('open') is not None:
                    open_times.append(time.get('open'))
        if query_num is not None:
            return jsonify(open_times[0:query_num])

        return jsonify(open_times)

class List_open_csv(Resource):
    def get(self):
        parser.add_argument('top', type=int)

        list_f = list(collection.find())
        rows_list = [['brevet_dist','start_date','controls']]
        for db_record in list_f:
            db_record.pop('ObjectId')
            db_record.pop('_id')

            row = [db_record.pop('brevet_dist'), db_record.pop('start_date')]
            c = db_record.pop("controls")

            for time in c:
                if time.get('open') is not None:
                    row.append(time.get('open'))

        with open('brevets_open.csv', 'w', newline='') as file_o:
            csv_writer_o = csv.writer(file_o)
            csv_writer_o.writerows(rows_list)

        file_path = safe_join(cwd, 'brevets_open.csv')

        return send_file(file_path,
                        mimetype = "application/octet-stream",
                        as_attachment=True,
                        download_name = 'brevets_all.csv'
                        )



#close times
class List_close(Resource):
    def get(self):
        list_f = list(collection.find())
        close_times = []
        for db_record in list_f:
            c = db_record.pop("controls")
            for time in c:
                if time.get('close') is not None:
                    close_times.append(time.get('close'))
                               
        return jsonify(close_times)
        
class List_close_json(Resource):
    def get(self):
        parser.add_argument('top', type=int)
        query_num = parser.parse_args()["top"]

        list_f = list(collection.find())
        close_times = []
        for db_record in list_f:
            c = db_record.pop("controls")
            for time in c:
                if time.get('close') is not None:
                    close_times.append(time.get('close'))
        if query_num is not None:
            return jsonify(close_times[0:query_num])
            
        return jsonify(close_times)

class List_close_csv(Resource):
    def get(self):
        parser.add_argument('top', type=int)
        list_f = list(collection.find())
        rows_list = [['brevet_dist','start_date','controls']]
        for db_record in list_f:
            db_record.pop('ObjectId')
            db_record.pop('_id')

            row = [db_record.pop('brevet_dist'), db_record.pop('start_date')]
            c = db_record.pop("controls")

            for time in c:
                if time.get('close') is not None:
                    row.append(time.get('close'))

        with open('brevets_close.csv', 'w', newline='') as file_c:
            csv_writer_c = csv.writer(file_c)
            csv_writer_c.writerows(rows_list)

        file_path = safe_join(cwd, 'brevets_close.csv')

        return send_file(file_path,
                        mimetype = "application/octet-stream",
                        as_attachment=True,
                        download_name = 'brevets_all.csv'
                        )


### End of Schemas

###
# Endpoints
###

##json endponits

#all controls
api.add_resource(List_all, '/listAll')
api.add_resource(List_all_json, '/listAll/json')

#open times
api.add_resource(List_open, '/listOpenOnly')
api.add_resource(List_open_json, '/listOpenOnly/json')

#close times
api.add_resource(List_close, '/listCloseOnly')
api.add_resource(List_close_json, '/listCloseOnly/json')


##CSV endpoints
#all controls
api.add_resource(List_all_csv, '/listAll/csv')

# open times
api.add_resource(List_open_csv, '/listOpenOnly/csv')

# close times
api.add_resource(List_close_csv, '/listCloseOnly/csv')

### End of end points


app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
