"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """

    neg_test(brevet_dist_km,control_dist_km)
   
    control_dist_km = control_normialize(brevet_dist_km, control_dist_km)
   
    control_dist_parts = break_control_dist(control_dist_km) 
    times = sum_time_max(control_dist_parts)

    start_time = arrow.get(brevet_start_time).shift(hours=times['h'],minutes=times['m'])
    return start_time.isoformat()


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    neg_test(brevet_dist_km,control_dist_km)

    control_dist_km = control_normialize(brevet_dist_km, control_dist_km)

    if control_dist_km == 0:
       times = {'h':1,'m':0}
    elif (control_dist_km == 200 and brevet_dist_km == 200):
       times = {'h':13,'m':30}
    else:
       control_dist_parts = break_control_dist(control_dist_km)
       times = sum_time_min(control_dist_parts)

    

    closing_time = arrow.get(brevet_start_time).shift(hours=times['h'],minutes=times['m'])
    return closing_time.isoformat()
    
#This dict holds maximum speeds for opening time calculations from the rusa.org caluctation chart
# rows after the first the lowwer bound is exclusive 
#ie; a control at 100 km would return 34
max_speeds = {
   '0-200':34,
   '200-400':32,
   '400-600':30,
   '600-1000':28

}

#This dict holds minimum speeds for opening time calculations from the rusa.org caluctation chart
#simplifed rows 1-3 to 1 dict entery
#values over 600km the lowwer bound is exlusive
#ie; 
min_speeds = {
   '0-200':15,
   '200-400':15,
   '400-600':15,
   '600-1000':11.428
}


#private method to sperate the control distance for difering max speeds in open_time()
def break_control_dist(control_dist_km):
   control_dist_pieces = {
   '0-200':0,
   '200-400':0,
   '400-600':0,
   '600-1000':0
   }

   if control_dist_km > 600:
      control_dist_pieces['600-1000'] = control_dist_km - 600
      control_dist_km -= control_dist_pieces['600-1000']

   if control_dist_km > 400:
      control_dist_pieces['400-600'] = control_dist_km - 400
      control_dist_km -= control_dist_pieces['400-600']
   
   if control_dist_km > 200:
      control_dist_pieces['200-400'] = control_dist_km - 200
      control_dist_km -= control_dist_pieces['200-400']
   
   control_dist_pieces['0-200'] = control_dist_km

   return control_dist_pieces

#private method that rasies ValueError if control_dist_km > 120% of brevet_dist_km
#if control_dist_km > return brevet_dist_km to treat the overflow as if it was the race
#distance as specified in ACP Brevet Control Times Calculator
def control_normialize(brevet_dist_km, control_dist_km):
   if (control_dist_km > (brevet_dist_km * 1.2)):
      raise ValueError("Can not be over 20% longer than the theoretical distance b:" + str(brevet_dist_km) + " c: " + str(control_dist_km))
   elif(control_dist_km > brevet_dist_km):
      return brevet_dist_km
   else:
      return control_dist_km

def neg_test(brevet_dist_km, control_dist_km):
   if (brevet_dist_km < 0 ):
      raise ValueError('brevet_dist_km can not be negitive')
   elif(control_dist_km < 0):
      raise ValueError('control_dist_km can not be negitive')

def sum_time_min(control_dist_parts):
   hours = 0
   minutes = 0


   for key in control_dist_parts:
      hours += int(control_dist_parts[key] / min_speeds[key])
      minutes += round(((control_dist_parts[key] / min_speeds[key]) % 1*60) )
      
   hours += minutes // 60
   minutes = minutes % 60

   results = {"h": hours, "m": minutes}

   return results

def sum_time_max(control_dist_parts):
   hours = 0
   minutes = 0


   for key in control_dist_parts:
      hours += int(control_dist_parts[key] / max_speeds[key])
      minutes += round(((control_dist_parts[key] / max_speeds[key]) % 1*60) )
      
   hours += minutes // 60
   minutes = minutes % 60

   results = {"h": hours, "m": minutes}

   return results

