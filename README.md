# Project 6: Brevet time calculator service

This project is clone of the RUSA control calculator implemented with Python3, and Ajax, running on a Flask server for both page rendering and the API, that can store controls for latter access in MongoDB 

## Authors
- Credits to Michal Young for the initial version of this code.
- Base verison given by Prof. Ram Durairajan
- Final implementation by Thomas Jessop

## ACP control times

 Controls are points where over the course of a brevet a rider must obtain proof of passage, and control times are the minimum and maximum times by which the rider must arrive at the location.   

The algorithm for calculating controle times is described here (https://rusa.org/pages/acp-brevet-control-times-calculator). Note the 1000-1300 row is not impementated for it is not an official distance.

## Basic Usage
When the user comes to the main page they may enter a date, time, brevet distance, and the points for control in either miles or km.

Then the control open and close times will be calculated on the backend, then displayed without page reload.

The "Summit" button stores the calculated control open and close times into the database.

The "Display" button queries the database for stored control times and redirects the user to a page populated by the queried times or an error page if no data is currently stored in the database.

## Test Cases

### No Controls Entered
If the user clicks the "Sumbit" button and has not entered any miles or km to genterate at least one control they will be redirected to the empty post error page.

### Display an Empty Database
If the user clicks the "Display" button before having sumbitted to the database their will be redirected to the empty db error page.

Both error pages offer the user a "Go Back" button that redirects to the main page

## API Ussage
The API service exposes what is stored in the database.

# Base endpoints
- http://<host:port>/listAll  
    Returns all open and close times in the database
- http://<host:port>/listOpenOnly \
    Returns open times only
- http://<host:port>/listCloseOnly  \
    Returns close times only

# JSON representation endpoints

Largly the same as the base endpoints

- http://<host:port>/listAll/json\
    Returns all open and close times in JSON format
- http://<host:port>/listOpenOnly/json \
    Returns open times only in JSON format
- http://<host:port>/listCloseOnly/json\
    Returns close times only in JSON format

# CSV representation endpoints

Largly the same as the base endpoints

- http://<host:port>/listAll/csv\
    Returns all open and close times in CSV format
- http://<host:port>/listOpenOnly/csv \
    Returns open times only in CSV format
- http://<host:port>/listCloseOnly/csv\
    Returns close times only in CSV format

# Query parameter endpoints

 Theese endpoints get the oldest open and close times. For examples, see below.

- http://<host:port>/listOpenOnly/json?top=5\
    Returns first 5 open times to be entered into the database (in ascending order) in JSON format

- http://<host:port>/listCloseOnly/json?top=4\
    Returns first 4 close times to be entered into the database (in ascending order) in JSON format

## Architecture

This project uses a very simple miroservice architecture handled by Docker-compose.

There are two services the web service which holds the Flask server, and the db service that is the data layer implemented with MongoDB

## What is in this repository

### Controler / Page and API server implemented in Flask
- flask_brevets
- templates/
- static
- config

## Biz logic / Control time calculator
- acp_times

### Build Files
- Dockerfile
- docker_compose
- requirements


## Repo
[Bitbucket proj6-rest](https://bitbucket.org/Tljessop/proj6-rest/src/master/)

## Maintainer
Thomas Jessop
<tjessop2@uoregon.edu>